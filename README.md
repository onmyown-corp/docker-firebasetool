# Dockerized Firebase commande line tool
## Synopsis

Just a Dockerfile to have the Firebase Tools ready to use without any installation.

## References
### **Build**
Docker build commande:
```
	docker build -t "$DOCKER_IMG_NAME" .
```
### **Run**
Docker commnade to create a Firebase authentification token:
```
	docker run -it -p 9005:9005 $DOCKER_IMG_NAME login:ci
```
Docker commnade to initialise a Firebase hosting directory:
```
	docker run -it -v "$HOST_PATH:/home/firebase" $DOCKER_IMG_NAME init --token "$CI_TOKEN"
```
Docker commnade to deploy code to a Firebase hosting:
```
	docker run -it -v "$HOST_PATH:/home/firebase" $DOCKER_IMG_NAME deploy --token "$CI_TOKEN"
```
Docker commande to disable Firebase hosting:
```
	docker run -it $DOCKER_IMG_NAME hosting:disable --token "$CI_TOKEN" --project $PROJECT_ID
```

Variables:
- $DOCKER_IMG_NAME: is the Docker image name
- $CI_TOKEN: authentification token generated to access Firebase
- $HOST_PATH: code location on the host
- $PROJECT_ID: Firebase project ID

## Licence
This product is under MIT License.