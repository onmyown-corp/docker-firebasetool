FROM alpine:3.8
LABEL maintainer="florian.woerner@onmyown.io"

# specify nodejs environment variables
ENV NODE_ENV production
ENV NODE_PATH /home/firebase/node_modules

# create firebase volume
RUN mkdir /home/firebase
WORKDIR /home/firebase

# install base package && NodeJs && npm
RUN apk add --update --no-cache nodejs nodejs-npm openssl

# Specify where to install node_modules
RUN npm prefix $NODE_PATH

# install Firebase CMD Tool
RUN npm install -g firebase-tools

# expose port for login
EXPOSE 9005

ENTRYPOINT ["firebase"]
CMD ["--version"]